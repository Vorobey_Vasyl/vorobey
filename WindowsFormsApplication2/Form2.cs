﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {             
        private Brick[] recArray;
        private Button[] btnArray;               

        private Ball recBall;        
        private Padle recPaddle;
        private Random gen = new Random();
        private int mainSpeed = 0;
        private int xSpeed = 0;
        private int ySpeed = 0;

        Form1 form1;
        public Form2(Form1 form1)
        {
            this.form1 = form1;
            InitializeComponent();
            xSpeed = 1;
            ySpeed = 2;
            mainSpeed = 5;
            
            recArray = new Brick[49];
            btnArray = new Button[49];

            recBall = new Ball(btnBall.Location.X, btnBall.Location.Y, btnBall.Width, btnBall.Height);
            recPaddle = new Padle(btnPaddle.Location.X, btnPaddle.Location.Y, btnPaddle.Width, btnPaddle.Height);
                    
            int k = 0; int i = 0; int j = 0; int l = 0;
            int sx = 40; int sy = 40;
            int lef = 80; int bot = 40;
            int px = 10; int py = 20;
            Color color;
            color = Color.ForestGreen;
            Button[] b = new Button[49];
            while (k<49)            
            {
                if (k == 10)
                {
                    j++;
                    l = 0;
                    color = Color.YellowGreen;
                                  
                }
                if (k==20)
                {
                    j++;
                    l = 0;
                    color = Color.GreenYellow;
         
                    
                }
                if (k == 30)
                {

                    j++;
                    l = 0;
                    color = Color.Khaki;
      
                }
                if (k == 40)
                {

                    j++;
                    l = 0;
                    px = 50;
                    color = Color.WhiteSmoke;
                    
                }
               
                b[i]=new Button();

                b[i].Name = "b1"; 
                b[i].Parent = this;                      
                b[i].BackColor= color;
                b[i].Size = new Size(sx, sy);
                b[i].Location = new Point(px+l*lef, py+bot*j); 
                b[i].FlatStyle = FlatStyle.Standard;              
                b[i].Enabled = false; 
                i++;
                k++;
                l++;
            }
           
            for (i = 0; i < 49; i++)
            {            
                                   
               recArray[i] = new Brick(b[i].Location.X, b[i].Location.Y, b[i].Width, b[i].Height);                
               btnArray[i] = b[i];               
            }
                    
        }

        private void Form2_MouseMove(object sender, MouseEventArgs e)
        {
          
            if (e.X >= 65 && e.X <= 783-63)
            {
                this.btnPaddle.Location = new System.Drawing.Point(e.X - btnPaddle.Width / 2, 524);
            }
        }
        int k = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
           
            btnBall.Location = new System.Drawing.Point(btnBall.Location.X + xSpeed, btnBall.Location.Y + ySpeed);
                        
            recPaddle.Location1(btnPaddle.Location);
            recBall.Location1(btnBall.Location); 
            for (int i = 0; i < (recArray.Length); i++ )
            {
                if (btnArray[i].Visible == true && recArray[i].MyobjectRec.IntersectsWith(recBall.MyobjectRec))
                {
                    ySpeed = mainSpeed;
                    btnArray[i].Visible = false;
                    k++;                   
                }
            }
            if (k == 49)
            {
                label1.Visible = true;
                xSpeed = 0;
                ySpeed = 0;
                btnBall.Visible = false;
                button1.Visible = true;
                button1.Enabled = true;
                button2.Visible = true;
                button2.Enabled = true;
            }

            if (recBall.MyobjectRec.IntersectsWith(recPaddle.MyobjectRec)) ySpeed = -mainSpeed;
            else if (btnBall.Location.X >= 745) xSpeed = -mainSpeed;
            else if (btnBall.Location.X <= 1) xSpeed = mainSpeed;
            else if (btnBall.Location.Y <= 1) ySpeed = mainSpeed;
            else if (btnBall.Location.Y >= 524)
            {
                ySpeed =0;
                xSpeed = 0;
                btnBall.Visible = false;
                btnBall.Location = new Point(346, 312);
                label2.Visible = true;
                button1.Visible = true;
                button1.Enabled = true;
                button2.Visible = true;
                button2.Enabled = true;
            }

        }
        bool w = true;
        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
           if (w==true) form1.Show();
        }
        
       
        private void button1_Click(object sender, EventArgs e)
        {
            w = false;
            Close();
            form1.button1_Click(sender, e);
            
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            w = true; 
            Close();
        }
        
    }
}

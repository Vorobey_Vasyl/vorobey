﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    class Myobject
    {
        protected Rectangle myobjectRec;             

        public Rectangle MyobjectRec
        {
            get { return myobjectRec; }

        }

        public void Location1(Point Location)
        {
            myobjectRec.Location = Location;

        }
    }
}
